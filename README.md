# jsCallbackAndCo

Fonction de rappel, opération asynchrone et promesse...

## Exercice 1
Créer une fonction "callback" qui permettra d'afficher le nom renseigné depuis la boite de dialogue "prompt" lors de l'éxécution de la fonction décrite ci-dessous : 

```
function processUserInput(callback) {
  var name = prompt('Entrez votre nom');
  callback(name);
}
```

## Exercice 2
Demander à l'utilisateur l'autorisation de recevoir des notifications :

```
Notification.requestPermission()
```

Créer une fonction de "callback" permettant d'afficher une notification avec le message "ha ha, je vais te spammer !"

## Exercice 3
La même et on recommence mais avec la version promesse... Hey ca va bien se passer, c'est promis !

## Exercice 4
Sans les mains, qu'est-ce qui sera affiché dans la console ?

```
fetch('http://example.com/movies.json')
  .then(function(response) {
    return response.json();
  })
  .then(function(json) {
    console.log( JSON.stringify(json) );
  });
```

## Exercice 5
Et car vous êtes des artistes... encore sans les mains, qui est votre préféré ?

```
let firstname = 'jerome';

function whoIsYourFavorite(firstname = 'stephane') {
    alert(firstname);
}

whoIsYourFavorite();
```